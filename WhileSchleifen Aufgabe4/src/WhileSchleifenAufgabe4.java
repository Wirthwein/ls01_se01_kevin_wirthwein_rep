import java.util.Scanner;

public class WhileSchleifenAufgabe4 
{

	public static void main(String[] args) 
	{
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben sie einen Startwert in Celsius ein: ");
		
		int Start = tastatur.nextInt();
		
		System.out.println("Bitte geben sie ienen Endwert in Celsius ein: ");
		
		int Ende = tastatur.nextInt();
		
		System.out.println("Bitte geben sie eine Schrittweite in Celsius ein: ");
		
		int Schritt = tastatur.nextInt();
		while(Start <= Ende)
		{
			double Fahrenheit = (Start * 1.8) + 32;
				
			System.out.println(Start + "�C" + "\t" + Fahrenheit + "�F");
			Start = Start + Schritt;
			
		}
	}

}
