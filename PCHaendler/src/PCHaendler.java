import java.util.Scanner;

public class PCHaendler 
{

	public static void main(String[] args)
	{
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("was m�chten Sie bestellen?");
		String artikel = myScanner.next();
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();
		System.out.println("Geben Sie den Nettopreis ein:");
		double erg1 = 0;
		double erg2 = 0;
		double preis = myScanner.nextDouble();
		double mwst = 0.19;
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = (nettogesamtpreis * mwst) + nettogesamtpreis;
		Rechnungen(anzahl, preis, nettogesamtpreis, mwst, erg1, erg2);
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
	public static double Rechnungen(int anzahl, double nettopreis,double nettogesamtpreis, double mwst, double erg1, double erg2)
	{
		erg1 = anzahl * nettopreis;
		erg2 = nettogesamtpreis * mwst;
		
		return erg2 + erg1;
	}
	public static void rechungausgeben(String artikel,int anzahl,double nettogesamtpreis,double bruttogesamtpreis,double mwst)
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.2f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		System.out.printf("");	
	}

}
