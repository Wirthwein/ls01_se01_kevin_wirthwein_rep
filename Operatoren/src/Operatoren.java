﻿/* Operatoren.java
   Uebung zu Operatoren in Java
   @author
   @version
*/
public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/
int zahl1 = 5, zahl2 = 2, zahl3;
zahl3 = zahl1 + zahl2;
System.out.println(zahl3);
    System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */
int zahl4 = 75, zahl5 = 23;
    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
int zahl6;
zahl6 = zahl4 + zahl5;
System.out.println(zahl6);
    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */
int zahl7,zahl8, zahl9, zahl10;
	zahl7 = zahl4 - zahl5;
	zahl8 = zahl4 * zahl5;
	zahl9 = zahl4 / zahl5;
	zahl10 = zahl4 % zahl5;
System.out.println("zahl4 - zahl5 = " + zahl7 + "\n" + "zahl4 * zahl5 = " + zahl8 + "\n" + "zahl4 / zahl5 = " + zahl9 + "\n" + "zahl4 % zahl5 = " +zahl10);
    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
if (zahl4 == zahl5)
{
	System.out.println("die zahlen sind gleich");
	
}
else
{
	System.out.println("die zahlen sind nicht gleich");
}	
    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */

    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
          
  }//main
}// Operatoren
