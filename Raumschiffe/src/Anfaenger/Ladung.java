package Anfaenger;

public class Ladung 
{
	private String bezeichnung;
	private int menge;
	
	public Ladung()
	{
		this.bezeichnung = "unknown";
		this.menge = 0;
	}
	public String getBezeichnung()
	{
		return bezeichnung;	
	}
	public void setBezeichnung(String bezeichnung)
	{
		this.bezeichnung = bezeichnung;
	}
	public int getMenge()
	{
		return menge;
	}
	public void setMenge(int menge)
	{
		this.menge = menge;
	}
	
}
