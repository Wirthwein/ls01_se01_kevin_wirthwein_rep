package Anfaenger;

public class LadungTest {

	public static void main(String[] args) {
		
		Raumschiff r1 = new Raumschiff();
		
		r1.setEnergieversorgungInProzent(100);
		r1.setHuelleInProzent(100);
		r1.setLebenserhaltungssystemeInProzent(100);
		r1.setSchildeInProzent(100);
		r1.setAndroidenAnzahl(5);
		r1.setphotonentorpedoAnzahl(4);
		
		System.out.print("Androiden: ");
		System.out.println(r1.getAndroidenAnzahl());
		System.out.print("Torpedos: ");
		System.out.println(r1.getphotonentorpedoAnzahl());
		System.out.print("Energieversorgung: ");
		System.out.print(r1.getEnergieversorgungInProzent());
		System.out.println("%");
		System.out.print("HuelleInProzent: ");
		System.out.print(r1.getHuelleInProzent());
		System.out.println("%");
		System.out.print("LebenserhaltungssystemeInProzent: ");
		System.out.print(r1.getLebenserhaltungssystemeInProzent());
		System.out.println("%");
		System.out.print("SchildeInProzent: ");
		System.out.print(r1.getSchildeInProzent());
		System.out.println("%");
		
		r1.photonentorpedoSchiessen(r1);
		
		
	}

}
