import java.util.Scanner;

public class WhileSchleifeBeispiele {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		int bisZahl = tastatur.nextInt();
		int n = 1;
		while(n <= bisZahl)
		{
			System.out.println(n);
			n++;
		}
	}

}
