
public class Mittelwert
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
	      // (E) "Eingabe"
	      // Werte f�r x und y festlegen:
	      // ===========================
	      double x = 2.0;
	      double y = 4.0;
	      double m;
	      
	      // (V) Verarbeitung
	      // Mittelwert von x und y berechnen: 
	      // ================================
	      m = (x + y) / 2.0;
	      
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================
	      printMittelwert(x,y,m);
	      
	}
	
	
	public static void printMittelwert(double y, double x, double m)
	{
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
		
	}
	
	
	public static double berechneMittelwert(double x, double y) 
	{
		double erg = (y + x) / 2.0;
		return erg;
		
	}

}
