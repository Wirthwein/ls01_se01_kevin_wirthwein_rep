
public class Aufgabe2 {

	public static void main(String[] args) {
		
		System.out.printf("%s %-19s %4s %s%n", "0!", "= ", "=", "1"); 
		System.out.printf("%s %-19s %4s %s%n", "1!", "= 1", "=", "1"); 
		System.out.printf("%s %-19s %4s %s%n", "2!", "= 1 * 2", "=", "2"); 
		System.out.printf("%s %-19s %4s %s%n", "3!", "= 1 * 2 * 3", "=", "6"); 
		System.out.printf("%s %-19s %4s %s%n", "4!", "= 1 * 2 * 3 * 4", "=", "24"); 
		System.out.printf("%s %-19s %4s %s%n", "5!", "= 1 * 2 * 3 * 4 * 5", "=", "120"); 
		

	}

}
