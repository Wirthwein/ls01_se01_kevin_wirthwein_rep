import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {
    do
    {
    	Scanner tastatur = new Scanner(System.in);

        int anzahlTickets;
        double zuZahlenderBetrag;
        double ticketPreis = 0;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;
        
        

        
        System.out.println("W�hlen sei eines der Drei Tickets aus.");
        System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");		
        System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
       	anzahlTickets = tastatur.nextInt();
       	switch(anzahlTickets)
       	{
       		case 1:
       			System.out.println("sie haben den Einzelfahrschein gew�hlt");
       			ticketPreis = 2.90;
       			break;
       		case 2:
       			System.out.println("sie haben die Tageskarte gew�hlt");
       			ticketPreis = 8.60;
       			break;
       		case 3:
       			System.out.println("sie haben die Kleingruppen-Tageskarte gew�hlt");
       			ticketPreis = 23.50;
       			break;
       	}
       	
        
        while(anzahlTickets > 4 || anzahlTickets < 0)
        {
        
        	System.out.println("ihre gew�hlte anzahl entspricht nicht dem zul�ssigen bereich");
            System.out.print("Anzahl der Tickets: ");
           	anzahlTickets = tastatur.nextInt();
            
        }
        
        zuZahlenderBetrag = ticketPreis * anzahlTickets;

        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f �%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rueckgabebetrag > 0.0) {
            //***** L�sung der Ausgabenformatierungsaufgabe *****************/
            System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir w�nschen Ihnen eine gute Fahrt.\n");
    }while(true);
    }
}