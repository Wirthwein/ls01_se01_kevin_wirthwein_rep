/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  9;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 78235783789L;
    
    // Wie viele Einwohner hat Berlin?
    float bewohnerBerlin  = 3.769f; 
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 7300;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 190000;   
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int flaecheGroessteLand = 17098242; 
    
    // Wie groß ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Anzahl der Anwohner in Berlin: " + bewohnerBerlin + "mio");
    System.out.println("Anzahl der ungef�hren Tage meines Alters: " + alterTage);
    System.out.println("Gewicht des schwersten Tieres: " + gewichtKilogramm + "kg");
    System.out.println("Fl�che des gr��ten Landes: " + flaecheGroessteLand + "km�");
    System.out.println("Fl�che des kleinsten Landes: " + flaecheKleinsteLand + "km�");
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

