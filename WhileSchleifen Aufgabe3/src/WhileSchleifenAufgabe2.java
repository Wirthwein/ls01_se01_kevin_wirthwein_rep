import java.util.Scanner;

public class WhileSchleifenAufgabe2 
{

	public static void main(String[] args) 
	{
	
		int Quersumme = 0;
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.printf("Zahl eingeben: ");
		
		int Zahl = tastatur.nextInt();
		
		
		while (Zahl != 0)
		{
			
			Quersumme += Zahl % 10;

			Zahl = Zahl / 10;
		
		}
		
			System.out.println("Die Quersumme ergibt: " + Quersumme);
		
	}
}
